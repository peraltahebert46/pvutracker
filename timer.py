import requests
import time
import random
import json

class Request():

    def __init__(self, endpoint):
        self.endpoint = endpoint
        self.send_request()

    def __str__(self) -> str:
        resp = self.send_request()
        return resp

    def send_request(self):
        token= input("\nElige el Token a usar:\n1.-Token Grupo 1\n2.-Token Grupo 2\n")
        if token == "1":
            token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHgzZWI1YjJiNDBiZWY3OWFlODViYzAzM2RmZTMxYTA4OThkM2I0ZmE2IiwibG9naW5UaW1lIjoxNjI5NTcyNjg3NDgyLCJjcmVhdGVEYXRlIjoiMjAyMS0wOC0yMSAwMDoyMDowMyIsImlhdCI6MTYyOTU3MjY4N30.pkL1m5NT5KGmjTg4Qs1wCZ0Jm4yC6O7MUPLg-p6GL2g"
        else:
            token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHhlZDNkOGNmYWE1NTQ5ZDkxNDBjNWUxMWVhMGI4MTBiZGE5YzgxMzdiIiwibG9naW5UaW1lIjoxNjI5NTc5MDc1NzExLCJjcmVhdGVEYXRlIjoiMjAyMS0wOC0yMSAyMDoxMTozOSIsImlhdCI6MTYyOTU3OTA3NX0.2RYdWXyMDowZLd-zQP2EB-sEJdtK5tQqZ3WKYpGNuuk"
        while True:
            print('\nSending request to {}'.format(self.endpoint))
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
                'method': 'GET',
                'authority': 'backend-farm.plantvsundead.com',
                'path': 'backend-farm.plantvsundead.com',
                'scheme': 'https',
                'accept': 'application/json',
                'authorization': 'Bearer Token: {}'.format(token)
            }
            # Grupo1 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHgzZWI1YjJiNDBiZWY3OWFlODViYzAzM2RmZTMxYTA4OThkM2I0ZmE2IiwibG9naW5UaW1lIjoxNjI5NTcyNjg3NDgyLCJjcmVhdGVEYXRlIjoiMjAyMS0wOC0yMSAwMDoyMDowMyIsImlhdCI6MTYyOTU3MjY4N30.pkL1m5NT5KGmjTg4Qs1wCZ0Jm4yC6O7MUPLg-p6GL2g
            # MetamaskMio eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHg1ZmI1ZjgwOWQ1YTlhYjgwOTE0NzdhY2I2ZGRiZjkyNGM3OTYyMzcwIiwibG9naW5UaW1lIjoxNjI5NDcxOTI0MDcwLCJjcmVhdGVEYXRlIjoiMjAyMS0wNy0zMSAxNzozMzo1NiIsImlhdCI6MTYyOTQ3MTkyNH0.-8r9FBZwgdjj2we4Z0lcznWzmVhIYMQRMhcFpVr5mTs
            # Grupo2 posiblemente eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwdWJsaWNBZGRyZXNzIjoiMHhlZDNkOGNmYWE1NTQ5ZDkxNDBjNWUxMWVhMGI4MTBiZGE5YzgxMzdiIiwibG9naW5UaW1lIjoxNjI5NTc5MDc1NzExLCJjcmVhdGVEYXRlIjoiMjAyMS0wOC0yMSAyMDoxMTozOSIsImlhdCI6MTYyOTU3OTA3NX0.2RYdWXyMDowZLd-zQP2EB-sEJdtK5tQqZ3WKYpGNuuk
            query = requests.get(self.endpoint, headers=headers)
            print(query.content)
            if query.status_code == 200:
                resp = query.json()
                if resp['status'] == 444:
                    print('Not in whitelist')
                    time.sleep(2)
                else:
                    print('In whitelist')
                    # Start a new method from the class Request to process the variable "resp"
                    break
            else:
                print('Retrying...')

query = Request('https://backend-farm.plantvsundead.com/farms/other/0x705cf72d8f4bf88cd225d8cc2e62df9fa6aaee52?limit=10&offset=0')
